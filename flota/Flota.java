import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Flota{
    private static List<Coche> coches = new ArrayList<Coche>();
    private static String[] marcas = {"Renault", "Dacia", "Seat", "Citroen"};
    private static String[] colores = {"Rojo", "Verde", "Azul", "Blanco", "Negro"};

    public static void generaCoches(int cuantos){
        Random rnd = new Random();
        for (int i = 0 ; i<cuantos; i++){
            Flota.coches.add(new Coche(
                Flota.marcas[rnd.nextInt(Flota.marcas.length)],
                Flota.colores[rnd.nextInt(Flota.colores.length)],
                rnd.nextInt(20)+2000,
                rnd.nextInt(150000)
            ));
        }
    }

    public static void info(){
        int n = 0;
            for(Coche c : Flota.coches){
                if(c.getKms()>100000){
                    n++;
                }
            }
            System.out.printf("%d coches de mes de 100k.\n", n);

            Map<String, AtomicInteger> marcasMap = new TreeMap<String, AtomicInteger>();
            for(Coche c : Flota.coches){
                AtomicInteger count = marcasMap.get(c.getMarca());
                if (count == null) {
                    count = new AtomicInteger(0);
                    marcasMap.put(c.getMarca(), count);
                }
                count.incrementAndGet();
            }

            String marcaMax = "";
            int max = 0;

            for (Entry<String, AtomicInteger> entry : marcasMap.entrySet()) {
                int numCoches = entry.getValue().get();
                System.out.print("Ocurrencias de " + entry.getKey() + ": ");
                System.out.println(numCoches);
                if (numCoches > max){
                    max = numCoches;
                    marcaMax = entry.getKey();
                }
            }

            System.out.printf("La marca mas vendida es: %s (%d)\n", marcaMax, max);

        }


}