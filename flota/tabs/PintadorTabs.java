package tabs;

public class PintadorTabs {

    static String pintaTab(Pintable cosa){

        
        if (cosa instanceof Producto){
            Producto p1 = (Producto) cosa;
            return p1.getDescription();
        } else if (cosa instanceof Factura) {
            Factura f1 = (Factura) cosa;
            return f1.getNumeroFactura();
        } else if (cosa instanceof Cliente) {
            return ((Cliente) cosa).getNombre();
        } else {
            return "Error";
        }

    }

}