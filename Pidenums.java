
import java.util.Scanner;

class Pidenums {

    public static void pide() {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int min = 0;
        int num = 0;
        int max = 0;
        int contador = 0;
        double med = 0;
        boolean primeravez = true;
        
        
        do {
            System.out.printf("Entra un num: ");
            try {
                num = keyboard.nextInt();
                if (primeravez){
                    max=num;
                    min=num;
                    primeravez=false;
                }
                if (num!=0){
                    contador++;
                    total += num;
                    if (num>max || max==0) max=num;
                    if (num<min || min==0) min=num;
                }
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num != 0);

        med = 1.0 * total / contador;

        System.out.println("La suma es " + total);
        System.out.println("El máximo es " + max);
        System.out.println("La mínimo es " + min);
        System.out.println("La media es " + med);
        keyboard.close();

    }

}
