
public class Company extends Object {
    private String name;
    private int year;

    @Override
    public int hashCode() {
        int miHash = this.name.hashCode() * this.year;
        return miHash;
    }

    @Override
    public boolean equals(Object obj) {
        Company otra = (Company) obj;
        
        if( this.name.equals(otra.name) && this.year==otra.year){
            return true;
        }else{
            return false;
        }
    }
    

    public Company(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}