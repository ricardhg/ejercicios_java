import java.util.ArrayList;
import java.util.Arrays;<
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ListTest{

    public static void main(String[] args) {

        List<Integer> items = Arrays.asList(1,2,3,4,5);
        
        List<Integer> items2 = new ArrayList<Integer>();
        items2.add(1);
        items2.add(2);
        items2.add(3);
        items2.add(4);
        items2.add(5);
        

        System.out.println(items.get(1));

        AtomicInteger x = new AtomicInteger(0);
        for (Integer i : items ){
            if (i%2!=0){
                x.addAndGet(i);
            }
        }

        int y = 0;
        for (Integer i : items ){
            if (i%2!=0){
                y = y+i;
            }
        }
        
        System.out.println(x.toString());

    }
}