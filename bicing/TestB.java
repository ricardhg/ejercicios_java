import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class TestB {

    public static void main(String[] args) {
   

            String resp = TestB.conecta();
            if (resp.equals("Ok")){
                System.out.println("Todo ha ido bien, archivo testb.json creado");
            } else {
                System.out.println("Algo ha fallado");
            }
      
    }

    public static String conecta() {

        File dest = new File("testb.json");
        URLConnection conn = null;

        try {
            URL url = new URL("https api.citybik.es/v2/networks/bicing");
            conn = url.openConnection();


        // } catch (MalformedURLException e) {
        //     // System.out.println("url no valida");
        //     return "Nok";
        } catch (IOException e) {
            // System.out.println("pagina no responde");
            return "Nok";
        }



        
        try (
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            FileOutputStream fos = new FileOutputStream(dest);
            BufferedOutputStream bos = new BufferedOutputStream(fos);) {
        int size = 1024;
        byte[] buffer = new byte[size];
        int len;
        while ((len = bis.read(buffer, 0, size)) > -1) {
            bos.write(buffer, 0, len);
        }
        bos.flush(); //ojo, no es imprescindible segun documentación de java jdk
    } catch (IOException e) {
        // System.out.println("error recibiendo o escribiendo datos");
        return "Nok";
    }

        return "Ok";
    }

}