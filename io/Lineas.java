import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Lineas {
	public static void main(String[] args) {
		if (args.length==0) {
			System.out.println("Archivo no especificado.");
			return;
		}
		String archivo = args[0].toLowerCase();
		File fin= new File(archivo);
		int lineas = 0;
		try (	FileReader fr = new FileReader(fin);
				BufferedReader br = new BufferedReader(fr);
				) {
			String line;
			do {
				line = br.readLine();
				if (line!=null) {
					//System.out.println(line);
					lineas++;
					}
			} while (line!=null);
	
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.printf("El arhivo %s tiene %d lineas.", archivo, lineas);
	}
}