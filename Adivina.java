
//package a importar al principio del archivo de clase
import java.util.Random;
import java.util.Scanner;


class Adivina{

    public static void adivina(int limite){
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(limite) + 1;
        int num=0;
        int intentos =0;
       
        System.out.printf("Entra un num del 1 al %d :", limite);
        do {
            try {
                num = keyboard.nextInt();
                intentos++;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto ***");
                keyboard.next();
            }
            
            if (num>incognita ){
                System.out.printf("Te has pasado. Intenta otra vez.");
            }else if(num<incognita) {
                System.out.printf("Te has quedado corto. Intenta otra vez.");
            } else {
                System.out.printf("Te ha costado %d intentos\n", intentos);
            }

        } while (num != incognita);
        
        keyboard.close();
    }


}
