
package com.enfocat.java;

class Coche extends Vehiculo {

    public int cilindrada;

    public Coche(int cm3, int ruedas){
        super(ruedas);
        this.cilindrada = cm3;
    }

    public Coche(int cm3){
        this(cm3, 4);
    }

}