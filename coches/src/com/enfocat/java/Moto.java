package com.enfocat.java;

class Moto extends Vehiculo {

    public int potencia;

    public Moto(int cv, int ruedas){
        super(ruedas);
        this.potencia = cv;
    }

    public Moto(int cv){
        this(cv, 2);
    }

}