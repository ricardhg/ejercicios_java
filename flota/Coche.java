

class Coche{
    private int anyo;
    private int kms;
    private String marca;
    private String color;

    public Coche(String marca, String color, int anyo, int kms){
        this.marca = marca;
        this.color = color;
        this.anyo = anyo;
        this.kms = kms;
    }

    @Override
    public String toString() {
        return String.format("%d %s %s (%d)", this.anyo, this.marca, this.color, this.kms);
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}