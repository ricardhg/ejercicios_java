

class Estacio {

    public int bikes;
    public String name;
    public int idx;
    public int lat;
    public String timestamp;
    public int lng;
    public int id;
    public int free;
    public int number;

    @Override
    public String toString() {
        return this.name + " ("+this.lat+", "+this.lng+")";
    }
    
}