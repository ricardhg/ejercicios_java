/**
 * clase persona nombre...
 * 
 * @author ricard
 */
public class Persona {

    public String nombre;
    public int edad;

    public Persona(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    @Override
    public String toString() {
        return this.nombre+" ("+this.edad+")";
    }

    @Override
    public boolean equals(Object ob){
        Persona otro = (Persona) ob;
        if(this.edad==otro.edad && this.nombre.equals(otro.nombre)){
            return true;
        }else{
            return false;
        }
    }


}