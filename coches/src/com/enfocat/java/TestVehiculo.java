package com.enfocat.java;

import com.enfocat.java2.Persona;

class TestVehiculo {

    public static void main(String[] args) {
        Vehiculo v = new Coche(1000);
        Coche c =  (Coche) v;
        Moto m = new Moto(100);
        Moto mp3 = new Moto(40,3);

        System.out.println("Coche");
        System.out.println(c.numruedas);
        System.out.println(c.cilindrada);
        cambiaRuedas(c);

        System.out.println("Moto");
        System.out.println(m.numruedas);
        System.out.println(m.potencia);
        cambiaRuedas(m);

        Persona.saluda();
    }

    public static void cambiaRuedas(Object o){
        Vehiculo v = (Vehiculo) o;
        System.out.printf("Cambiando %d ruedas\n\n", v.numruedas);
    }

}